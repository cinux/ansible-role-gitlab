# frozen_string_literal: true

# Molecule managed

describe apt('https://packages.gitlab.com/gitlab/gitlab-ce/debian/') do
  it { should exist }
  it { should be_enabled }
end

describe package('gpg') do
  it { should be_installed }
end

describe package('gitlab-ce') do
  it { should be_installed }
end

[ 
  'nginx[\'enable\'] = false',
  "external_url 'https://gitlab.mydomain.com'",
  "web_server['external_users'] = [ \"nginx\" ]",
  "gitlab_rails['trusted_proxies'] = ['10.0.0.2/24']",
  "gitlab_rails['backup_path'] = \"/mnt/backup/gitlab\"",
  "gitlab_rails['backup_keep_time'] = 604800",
  "gitlab_rails['gitlab_email_from'] = 'from@mailprovider.com'",
  "gitlab_rails['gitlab_email_reply_to'] = 'from@mailprovider.com'",
  "gitlab_rails['gitlab_email_display_name'] = 'Gitlab'",
  "prometheus_monitoring['enable'] = false"
].each do |s|
  describe file('/etc/gitlab/gitlab.rb') do
    its('content') { should match(/s/) }
  end
end