# frozen_string_literal: true

# Molecule managed

describe apt('https://packages.gitlab.com/gitlab/gitlab-ce/debian/') do
  it { should exist }
  it { should be_enabled }
end

describe package('gpg') do
  it { should be_installed }
end

describe package('gitlab-ce') do
  it { should be_installed }
end
