# gitlab

Install and configureation of Gitlab-CE

## Requirements

none

## Role Variables

`external_urls`: [external_urls](https://docs.gitlab.com/omnibus/settings/configuration.html#configure-the-external-url-for-gitlab)
`gitlab_rails_gitlab_email_from` [gitlab_rails['gitlab_email_from']](https://docs.gitlab.com/omnibus/settings/smtp.html)
`gitlab_rails_gitlab_email_display_name` [gitlab_rails['gitlab_email_display_name']](https://docs.gitlab.com/omnibus/settings/smtp.html)
`gitlab_rails_gitlab_reply_to` [gitlab_rails['gitlab_email_reply_to']](https://docs.gitlab.com/omnibus/settings/smtp.html)
`backup_path` [gitlab_rails['backup_gitaly_backup_path']](https://docs.gitlab.com/ee/raketasks/backup_restore.html)
`webserver_external_users` [web_server['external_users']](https://docs.gitlab.com/omnibus/settings/nginx.html#configuration)
`nginx_enabled` [nginx['enable']](https://docs.gitlab.com/omnibus/settings/nginx.html#configuration)
`prometheus_monitoring_enabled` [prometheus_monitoring['enable']](https://docs.gitlab.com/omnibus/settings/memory_constrained_envs.html#configuration-with-all-the-changes)
`gitlab_rails_trusted_proxies` [gitlab_rails['trusted_proxies']](https://docs.gitlab.com/omnibus/settings/nginx.html#using-a-non-bundled-web-server)

## Dependencies

None

## Example Playbook

```
    - hosts: servers
      roles:
         - { role: cinux.gitlab }
```

## License

MIT